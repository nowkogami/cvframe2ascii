from decimal import Decimal, ROUND_HALF_UP
import numpy as np
import cv2

class CvFrame2Ascii:
    """
    Converter from frame (returned from cv2.read()) to list of ascii strings

    Attributes
    ----------
    chars : list
        list of strings which corresponds to luminance of pixels in image
    size : tuple
        output size
        [0] is width (length of line)
        [1] is height (number of lines)
    capture : object
        cv2 capture object
    frame : numpy.ndarray
        snapshot frame
    """

    def __init__(self):
        """
        constructor
        """
        self.chars = ['  ', '. ', '..', '+.', '++', '*+', '**', '%*', '%%', '@%', '@@']
        self.size = (40, 25)
        self.lut = []
        self.capture = None
        self.frame = None

    def set_output_size(self, width, height):
        """
        Parameters
        ----------
        width : int
            length of line
        height: int
            number of lines
        
        Returns
        -------
        self : CvFrame2Ascii
        """
        self.size = (width, height)
        return self
    
    def set_capture(self, capture):
        """
        Parameters
        ----------
        capture : object
            cv2 capture object

        Returns
        -------
        self : CvFrame2Ascii
        """
        self.capture = capture
        return self
    
    def set_gamma(self, gamma):
        """
        Parameters
        ----------
        gamma : float
            gamma factor for luminance

        Returns
        -------
        self : CvFrame2Ascii
        """
        lookup_table = np.zeros((256, 1), dtype = 'uint8')
        for loop in range(256):
            lookup_table[loop][0] = 255 * pow(float(loop)/255, 1.0/gamma)
        self.lut = lookup_table
        return self

    def snapshot(self):
        """
        Execute snapshot to update frame

        Returns
        -------
        self : CvFrame2Ascii
        """
        if (self.capture is None):
            raise ValueError('capture is None')

        ret, f = self.capture.read()
        if (not ret):
            raise ValueError('failed to read frame')
        
        self.frame = np.copy(f)
        return self
    
    def convert(self):
        """
        Convert frame to list of ascii lines

        Returns
        -------
        lines : list
            list of ascii lines
        """
        if (self.frame is None):
            raise ValueError('frame is None')
        if (not all(self.size)):
            raise ValueError('size contains empty value')

        # resize
        line_width = self.size[0]
        image_width = int(Decimal(float(line_width) / 2.0).quantize(Decimal('1'), rounding=ROUND_HALF_UP))
        image_height = self.size[1]
        resized = cv2.resize(self.frame, (image_width, image_height))

        # grayscale
        gray = self.get_grayscale(resized)
        lines = []
        for src_line in gray:
            line = ''
            for luminance in src_line:
                s = self.convert_luminance2string(luminance, 255)
                line += s
            line_width = min([line_width, len(line)])
            lines.append(line[:line_width])
        lines.append('size=({}, {}), lines=({}, {})'.format(self.size[0], self.size[1], len(lines[0]), len(lines)))
        return lines

    def get_edge(self, image):
        edge = cv2.Canny(image, 50, 100)
        return edge

    def get_grayscale(self, image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if (len(self.lut) > 0):
            gray = cv2.LUT(gray, self.lut)
        return gray
    
    def convert_luminance2string(self, luminance, max_luminance):
        if (luminance > max_luminance):
            luminance = max_luminance
        index = int(Decimal((len(self.chars) - 1) * luminance / max_luminance).quantize(Decimal('1'), rounding=ROUND_HALF_UP))
        s = self.chars[index]
        return s
