from cvframe2ascii import CvFrame2Ascii
import cv2
import time

cap = cv2.VideoCapture(0)
gamma = 1.0
fno = 100
wait = 0.1
width = 32
height = 10

for fid in range(fno):
    print('fid='+str(fid))
    lines = CvFrame2Ascii().set_capture(cap).set_output_size(width, height).set_gamma(gamma).snapshot().convert()
    for line in lines:
        print(line)
    time.sleep(wait)