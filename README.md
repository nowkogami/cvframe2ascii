cvframe2ascii
====

python program to convert OpenCV captured frame to ascii strings

## Sample

```
fid=6
+.@%@@@%++@@%**++.++..++++. **%*
+.@%@@%%++++++@@@@@%%%++++@@+.%%
+.%%@%%*@%..+.. ++@%@%++. %%@%@%
+.%%%%+.++. +.+++.**@%+...@@@%%%
..%*%%%%..++******..@%+.. @%@%@%
..%*%%@%++++*+%*%%%%. ... @%@%@%
..****@%*+*+%*%*%%***+.   %*%%%*
. ++**+.@@++++%**+@@@@@%+.%*%%%*
. +...@%@@%%. *+. @@@@..+.*+%%**
+.@@. . @@@%++. @@@%@@%*..******
size=(32, 10), lines=(32, 10)
```

## Usage

```python:sample.py
#### continuous usage
from cvframe2ascii import CvFrame2Ascii
import cv2
import time

cap = cv2.VideoCapture(0)
gamma = 1.0
fno = 100
wait = 0.1

# render 100 frames to stdout
for fid in range(fno):
    print('fid='+str(fid))
    lines = CvFrame2Ascii().set_capture(cap).set_gamma(gamma).snapshot().convert()
    for line in lines:
        print(line)
    time.sleep(wait)
```

## Licence

[MIT](https://gitlab.com/nowkogami/cvframe2ascii/-/blob/master/LICENSE)

## Author

[nowkogami](https://gitlab.com/nowkogami)